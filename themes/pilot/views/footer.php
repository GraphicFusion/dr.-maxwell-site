<footer class="site-footer">
    <div class="inner-footer">
        <div class="col-7">
            <div class="app-links col-12">
                <p>Download Our App <a href="https://play.google.com/store/apps/details?id=com.app_maxwellaesthetics.layout"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/images/GooglePlay-Badge.svg" alt="Google Play App Link"/></a><a href="https://itunes.apple.com/us/app/maxwell-aesthetics/id1291980752?ls=1&mt=8"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/images/AppStore-Badge.svg" alt="Apple Store App Link"/></a></p>
            </div>
            <div class="footer-menu col-12">
                <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>

<!--                <ul class="footer-menu-list">-->
<!--                    --><?php //wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
<!---->
<!--                    <li class="footer-menu-list-item">Cosmetic Surgery</li>-->
<!--                    <li class="footer-menu-list-item">Medi Spa</li>-->
<!--                    <li class="footer-menu-list-item">Stories</li>-->
<!--                    <li class="footer-menu-list-item">About</li>-->
<!--                    <li class="footer-menu-list-item">Contact</li>-->
<!--                </ul>-->
            </div>
            <div class="col-12">
                <div class="contact-info-block col-3">
                    <p class="text-primary"><?php echo get_theme_mod('address_1_textbox'); ?></p>
                    <p class="text-primary"><?php echo get_theme_mod('address_2_textbox'); ?></p>
                </div>
                <div class="contact-info-block col-3">
                    <p class="text-primary">P: <?php echo get_theme_mod('phone_textbox'); ?></p>
                    <p class="text-primary">F: <?php echo get_theme_mod('fax_textbox'); ?></p>
                </div>
                <div class="contact-info-block col-6 col-last">
                    <ul class="contact-info-social-list">
                        <li class="contact-info-social-list-item col-3 text-align-center"><a
                                href="https://www.facebook.com/MaxwellAestheticsAZ/?fref=ts"><?php get_template_part('views/icons/facebook'); ?></a></li>
                        <li class="contact-info-social-list-item col-3 text-align-center"><a
                                href="https://twitter.com/Maxwell_MediSpa"><?php get_template_part('views/icons/twitter'); ?></a></li>
                        <!--li class="contact-info-social-list-item col-3 text-align-center"><a
                                href=""><?php get_template_part('views/icons/youtube'); ?></a></li>
                        <li class="contact-info-social-list-item col-3 col-last text-align-center"><a
                                href=""><?php get_template_part('views/icons/instagram'); ?></a></li -->
                    </ul>
                </div>
            </div>
            <div class="col-12">
                <div class="contact-info-block contact-info-block-last">
                    <p class="text-accent">
                        <span> &copy; <?php echo date("Y"); ?> <?php echo get_theme_mod('copyright_textbox'); ?></span>
                        <span> <?php printf(esc_html__('Design by %1$s', 'pilot'), '<a href="http://sonderagency.com/">Sonder</a>'); ?></span>
                    </p>
                </div>
            </div>

        </div>
        <div class="col-5 col-last text-align-right footer-image-container">
            <a href="/"><img class="site-footer-image" src="<?php echo preg_replace('#http:#','https:',get_theme_mod('footer_logo')); ?>"/></a>
        </div>
    </div>
</footer>