<?php
/**
 * string $args[0]['title']
 * string $args[0]['subtitle']
 * string $args[0]['content']
 * string $args[0]['bg_image_url']
 */
global $args;
?>
<div class="about-block-wrapper">
    <div class="col-4 inner-multi-content">
        <div class="side-image" style="background-image: url(<?php echo $args['bg_image_url']; ?>);">
        </div>
    </div>
    <div class="col-8 inner-multi-content float-right">
        <div class="title-wrap">
            <span><?php echo $args['title']; ?></span>
            <h3 class="subtitle"><?php echo $args['subtitle']; ?></h3>
            <div class="content"><?php echo $args['content']; ?></div>
        </div>
    </div>
</div>
