<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_about_layout(){
		$args = array(
			'title' => get_sub_field('about_block_title'),
			'subtitle' => get_sub_field('about_block_subtitle'),
			'content' => get_sub_field('about_block_content'),
		);
		$image = get_sub_field('about_block_image');
		if( is_array( $image ) ){
			$args['bg_image_url'] = $image['url'];
		}
		return $args;
	}

?>