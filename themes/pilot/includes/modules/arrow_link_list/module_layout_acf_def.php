<?php
global $pilot;
// add module layout to flexible content
$module_layout = array(
    'key' => '4f0cdd6ac8e1',
    'name' => 'arrow_link_list',
    'label' => 'Big Button',
    'display' => 'block',
    'sub_fields' => array(
        array (
            'key' => 'field_4f0cdd6ac8e4',
            'label' => 'Small List?',
            'name' => 'small_list',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => 100,
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
        ),
        array(
            'key' => 'field_4f0cdd6ac8e2',
            'label' => 'List Items',
            'name' => 'arrow_list_items',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => '1',
            'max' => '',
            'layout' => 'table',
            'button_label' => 'Add Item',
            'sub_fields' => array(
                array(
                    'key' => 'field_4f0cdd6ac8e3',
                    'label' => 'Link',
                    'name' => 'link',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 1,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '255',
                    'readonly' => 0,
                    'disabled' => 0,
                ))
        )
    ),
    'min' => '',
    'max' => '',
);

?>