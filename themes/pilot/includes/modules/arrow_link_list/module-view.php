<?php
/**
 * $args = array();
 */
global $args;
?>
<div class="arrow_link_list-wrap <?php echo $args['small_list'] ? 'small-list':'' ?>">
    <div class="arrow_link_list-inner-content">
        <?php foreach ($args['items'] as $item) : ?>
            <?php $title = get_the_title($item['link']->ID); ?>
            <a href="<?php echo get_permalink($item['link']->ID); ?>" class="arrow_link_list-link-container <?php if (is_page($title)) echo 'active'; ?>">
                <div class="arrow_link_list-link">
                    <div><?php echo $title ?></div>
                </div>
                <?php if (!$args['small_list']) : ?>
                    <div class="arrow_link_list-arrow">
                        <?php get_template_part('views/icons/arrow'); ?>
                    </div>
                <?php endif; ?>
            </a>
        <?php endforeach; ?>

    </div>
</div>
