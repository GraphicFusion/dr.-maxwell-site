<?php
if( function_exists('acf_add_options_sub_page') ) {
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Global Modules',
            'menu_title'    => 'Global Modules',
            'menu_slug'     => 'global-modules',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => 3.3,
            'icon_url'      => false
        )
    );    
}
global $pilot;
// add module layout to flexible content 
$global_module_layout = 
				array (
					'key' => '56ea09409c99c',
					'name' => 'global_block',
					'label' => 'Global Modules',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_56ea09452faf8',
							'label' => 'Choose your Global Module',
							'name' => 'global_block_predefined_block',
							'type' => 'select',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
							),
							'default_value' => array (
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'placeholder' => '',
							'disabled' => 0,
							'readonly' => 0,
						),
					),
					'min' => '',
					'max' => '',
				);

// gets the predefined blocks and builds a dropdown select of the,
function acf_load_global_blocks( $field ) {
	$field['choices'] = array('' => 'None');
	$rows = get_option('options_content_blocks');
	global $wpdb;
	if( is_array( $rows ) ){
		foreach( $rows as $key => $row ){	
			$row = preg_replace('/_block/','',$row);
			$array = $wpdb -> get_results(
				$wpdb->prepare( 
					"
						SELECT * 
						FROM wp_options
						WHERE option_name LIKE %s
					",
					'options_content_blocks_'.$key.'_'.$row.'_block_title'
				)
			);
			foreach( $array as $obj ){
				$field['choices'][$key] = $obj->option_value;
			}
		}
	}
	return $field;    
}
add_filter('acf/load_field/name=global_block_predefined_block', 'acf_load_global_blocks');

function build_global_layout(){
	// globals are stored in wp_options as 'options_content_blocks_{$global_key}_images_0_image'; 'global_block_defined_block' saves the $global_key
	$global_key = get_sub_field('global_block_predefined_block');
	$i = 0;	
	$args = [];
	if( have_rows('content_blocks' , 'option' ) ):
		while ( have_rows('content_blocks' , 'option') ): 
			the_row();
			if( $i == $global_key ):
				$test_block_type = preg_replace( '/_block/','',get_row_layout());
				$args = call_user_func('build_' . $test_block_type . '_layout');
				$args['global_type'] = preg_replace('/_/','-',$test_block_type);
				$args['global_func_name'] = preg_replace('/-/','_',$test_block_type);
			endif;
			$i++;
		endwhile;
	endif;
	if( count( $args )> 0 ){
		return $args;
	}
}

?>