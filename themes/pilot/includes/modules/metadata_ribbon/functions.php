<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_metadata_ribbon_layout()
{
    $args = array(
        'show_sharing' => get_sub_field('metadata_block_sharing'),
        'items' => get_sub_field('metadata_ribbon_rows')
    );
    return $args;
}

?>