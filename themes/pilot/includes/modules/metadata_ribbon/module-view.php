<?php
/**
 * $args = array();
 */
global $args;
?>
<div class="metadata_ribbon-wrap">
    <div class="metadata_ribbon-inner-content">
        <?php foreach ($args['items'] as $item) : ?>
            <div class="metadata_ribbon-inner-content-block"><span><?php echo $item['key']; ?>:</span> <span><?php echo $item['value']; ?></span></div>
        <?php endforeach; ?>
        <div class="metadata_ribbon-share-icon">
            <?php get_template_part('views/icons/share'); ?>
            <span>Share</span>
        </div>
    </div>
</div>
