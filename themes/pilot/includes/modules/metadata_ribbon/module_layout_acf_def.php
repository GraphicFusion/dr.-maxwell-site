<?php
global $pilot;
// add module layout to flexible content
$module_layout = array(
    'key' => '2551412f6336',
    'name' => 'metadata_ribbon',
    'label' => 'Credits',
    'display' => 'block',
    'sub_fields' => array(
        array(
            'key' => 'field_2551412f6337',
            'label' => 'Show Sharing?',
            'name' => 'metadata_block_sharing',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => 30,
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
        ), array(
            'key' => 'field_2551412f6338',
            'label' => 'Metadata',
            'name' => 'metadata_ribbon_rows',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => '1',
            'max' => '3',
            'layout' => 'table',
            'button_label' => 'Add Row',
            'sub_fields' => array(
                array(
                    'key' => 'field_2551412f6339',
                    'label' => 'Key',
                    'name' => 'key',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '255',
                    'readonly' => 0,
                    'disabled' => 0,
                ), array(
                    'key' => 'field_2551412f6340',
                    'label' => 'Value',
                    'name' => 'value',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '255',
                    'readonly' => 0,
                    'disabled' => 0,
                ))
        )
    ),
    'min' => '',
    'max' => '',
);

?>