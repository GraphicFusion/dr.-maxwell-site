<?php
/**
 * $args = array();
 */
global $args;
?>
<div class="three-image-grid-wrap">
    <div class="col-12 title">
        <?php if($args['title']!='') { ?>
        <h3><?php echo $args['title']; ?></h3>
        <?php } ?>
    </div>
    <div class="col-8 large-img-container">

        <div class="col-12 large-img" style="background-image:url(<?php echo $args['image_1']; ?>);">

            <div class="img-overlay" style="background-color:<?php echo $args['image_1_color']; ?>;opacity: <?php echo $args['image_1_opacity']; ?>">
            </div>
            <div class="img-content">
                <h4><?php echo $args['headline_1']; ?></h4>
                <a href="<?php echo $args['link_1']; ?>">MORE</a>
            </div>

        </div>

    </div>
    <div class="col-4 col-last small-img-container">
        <div class="col-12 col-last small-img" style="background-image:url(<?php echo $args['image_2']; ?>);">
            <div class="img-overlay" style="background-color:<?php echo $args['image_1_color']; ?>;opacity: <?php echo $args['image_1_opacity']; ?>"></div>

            <div class="img-content">
                <h4><?php echo $args['headline_2']; ?></h4>
                <a href="<?php echo $args['link_2']; ?>">MORE</a>
            </div>
        </div>

        <div class="col-12 col-last small-img" style="background-image:url(<?php echo $args['image_3']; ?>);">
            <div class="img-overlay" style="background-color:<?php echo $args['image_1_color']; ?>;opacity: <?php echo $args['image_1_opacity']; ?>"></div>
            <div class="img-content">
                <h4><?php echo $args['headline_3']; ?></h4>
                <a href="<?php echo $args['link_3']; ?>">MORE</a>
            </div>

        </div>

    </div>

</div>
