<?php
global $pilot;


function generateImageBlock($small, $idx)
{
    $size = $small ? "Small" : "Large";
    $modifyFieldId = "field_e1fd6822881f" . $idx;

    return array(
        array(
            'key' => "field_e1fd68228821" . $idx,
            'label' => "Image {$idx} ({$size})",
            'name' => "three_image_grid_image_{$idx}",
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => "field_e1fd68228822" . $idx,
            'label' => "Image {$idx} Headline",
            'name' => "three_image_grid_image_{$idx}_headline",
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => 33,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => "field_e1fd68228823" . $idx,
            'label' => "Image {$idx} Link",
            'name' => "three_image_grid_image_{$idx}_link",
            'type' => 'post_object',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => 33,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => $modifyFieldId,
            'label' => "Apply Filter to Image {$idx}",
            'name' => "three_image_grid_image_{$idx}_modify",
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => 33,
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
        ),
        array(
            'key' => "field_e1fd68228824" . $idx,
            'label' => 'Opacity',
            'name' => "three_image_grid_image_{$idx}_opacity",
            'type' => 'number',
            'instructions' => 'Set from 0 to 1 (for example 0.75)',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => $modifyFieldId,
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => 50,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 0,
            'max' => 1,
            'step' => '.01',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => "field_e1fd68228825" . $idx,
            'label' => 'Color',
            'name' => "three_image_grid_image_{$idx}_color",
            'type' => 'color_picker',
            'instructions' => 'Set the color overlay for the image or video',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => $modifyFieldId,
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => 50,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
        ),
    );
}

$subFields = array(
);
for ($i = 1; $i <= 3; $i++) {
    $small = $i > 1;
    $fields = generateImageBlock($small, $i);
    $subFields = array_merge($subFields, $fields);

}


// add module layout to flexible content
$module_layout = array(
    'key' => 'e1fd6822881d',
    'name' => 'three_image_grid',
    'label' => 'Feature Grid',
    'display' => 'block',
    'sub_fields' => $subFields,
    'min' => '',
    'max' => '',
);

?>