<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_three_image_grid_layout()
{
    $args = array(
        'title' => get_sub_field('three_image_grid_block_title'),
        'image_1' => get_sub_field('three_image_grid_image_1')['url'],
        'image_2' => get_sub_field('three_image_grid_image_2')['url'],
        'image_3' => get_sub_field('three_image_grid_image_3')['url'],
        'image_1_opacity' => get_sub_field('three_image_grid_image_1_opacity'),
        'image_2_opacity' => get_sub_field('three_image_grid_image_2_opacity'),
        'image_3_opacity' => get_sub_field('three_image_grid_image_3_opacity'),
        'image_1_color' => get_sub_field('three_image_grid_image_1_color'),
        'image_2_color' => get_sub_field('three_image_grid_image_2_color'),
        'image_3_color' => get_sub_field('three_image_grid_image_3_color'),
        'headline_1' => get_sub_field('three_image_grid_image_1_headline'),
        'headline_2' => get_sub_field('three_image_grid_image_2_headline'),
        'headline_3' => get_sub_field('three_image_grid_image_3_headline'),
        'link_1' => get_permalink(get_sub_field('three_image_grid_image_1_link')),
        'link_2' => get_permalink(get_sub_field('three_image_grid_image_2_link')),
        'link_3' => get_permalink(get_sub_field('three_image_grid_image_3_link')),
    );
    return $args;
}

?>