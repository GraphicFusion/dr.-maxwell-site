<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="gc-wrap">
	<?php if($args['title']!='') { ?>
	<h3><?php echo $args['title']; ?></h3>
    <?php } ?>
	<div class="gc-content"><?php echo $args['content']; ?></div>
</div>