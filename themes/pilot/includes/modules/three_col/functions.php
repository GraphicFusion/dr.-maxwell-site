<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if( file_exists ( $filename )){
    require $filename;
}

function build_three_col_layout(){
    $args = array(
        'columns' => get_sub_field('three_col_columns')
    );
    return $args;
}
?>