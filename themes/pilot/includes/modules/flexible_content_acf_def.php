<?php
	global $pilot;
	$pilot->flexible_content = array (
		'key' => 'group_567c3f9605502',
		'title' => 'Custom Content',
		'fields' => array (
			array (
				'key' => 'field_567c3f9c0e630',
				'label' => 'Content Blocks',
				'name' => 'content_blocks',
				'type' => 'flexible_content',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'button_label' => 'Add a Content Block',
				'min' => '',
				'max' => '',
				'layouts' => $pilot->layouts,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'global-modules',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	);
?>