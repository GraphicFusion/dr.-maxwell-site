<?php
	function get_additional_fields( $module ){
		global $pilot;
		$module = preg_replace('/-/','_',$module);
		$additional_fields[] = get_custom_class_field( $module );
		$additional_fields[] = get_global_class_field( $module );
		if( $pilot->use_colormaker ){
			$additional_fields[] = get_block_color_fields( $module );
		}		
		return $additional_fields;
	}
	function get_global_class_field( $module ){
		global $pilot;
		// add global classes
		$module_keys = get_field('modules_to_default','option');
		$default_value = 0;
		if( is_array( $module_keys ) ){
			foreach( $module_keys as $key ){
				$modules_to_default[] = $pilot->modules[$key];
			}
			if( in_array( $module , $modules_to_default ) ){
				$default_value = 1;
			}
		}
		$field = array (
			'key' => 'field_574910d0f7474'.$module,
			'label' => 'Check to Include Global Classes',
			'name' => $module.'_include_global_classes',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => $default_value,
		);
		return $field;
	}
	function get_title_field( $module){
		$field = array (
				'key' => 'field_56951fdc7c2f6'.$module,
				'label' => 'Title',
				'name' => $module.'_block_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
		return $field;

	}
	function get_custom_class_field( $module ){
		$field = array (
			'key' => 'field_56c60bf031ab6'.$module,
			'label' => 'Custom Classes',
			'name' => 'custom_classes_'.$module,
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
		return $field;
	};

	function get_block_color_fields( $module ){
		$field = array (
			'key' => 'field_568e6c04f59bb'.$module,
			'label' => 'Block Color',
			'name' => $module.'_block_color',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		);
		return $field;
	}
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_57490c3a885c0',
	'title' => 'Global Classes',
	'fields' => array (
		array (
			'key' => 'field_57490c4359fc3',
			'label' => 'Classes',
			'name' => 'global_classes',
			'type' => 'text',
			'instructions' => 'Space separated list of classes to apply by default to all modules. ',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_57490c7f59fc4',
			'label' => 'Modules to Default',
			'name' => 'modules_to_default',
			'type' => 'select',
			'instructions' => 'All modules will have a checkbox to include or exclude the global classes. Modules included in this list will have the box checked by default to include.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'' => 'None',
				0 => 'accordion',
				1 => 'alternating',
				2 => 'generic_content',
				3 => 'media',
				4 => 'multi',
				5 => 'posts-slider',
				6 => 'slider',
				7 => 'table',
				8 => 'three-column-image-grid',
				9 => 'woo_table',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'ui' => 1,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'global-modules',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


function acf_load_modules_to_default( $field ) {
	global $pilot;
	$field['choices'] = array('' => 'None');
	global $wpdb;
	if( is_array( $pilot->modules ) ){
		foreach( $pilot->modules as $key => $module ){	
			$field['choices'][$key] = $module;
		}
	}
	return $field;    
}
add_filter('acf/load_field/name=modules_to_default', 'acf_load_modules_to_default');

?>