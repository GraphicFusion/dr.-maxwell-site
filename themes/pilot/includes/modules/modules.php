<?php
require get_template_directory() . '/includes/modules/additional_acf_field_groups.php';

global $pilot;

$pilot->module_names = array();
$pilot->modules = get_modules();
$pilot->modules = array_merge($pilot->modules, $pilot->additional_modules); // $pilot->additional_modules are declared in functions.php
$pilot->layouts = array();        // will load acf layouts for modules

//TODO find a better way to do this, this moves the "inner-page" module to the end so we can pick up the other modules.
array_splice($pilot->modules, array_search('inner_page_content', $pilot->modules), 1);

foreach ($pilot->modules as $module) {
    $module_layout = array();
    $filename = get_template_directory() . '/includes/modules/' . $module . '/functions.php';
    // module's acf layout is built and added to $pilot->layouts[] in the following require
    if (file_exists($filename)) {
        require $filename;
        $additional_fields = get_additional_fields($module);
        foreach ($additional_fields as $field) {
            $module_layout['sub_fields'][] = $field;
        }
		$pilot->module_names[$module] = array(
            'class' => preg_replace('/_/', '-', $module),
            'key' => $module_layout['key'],
            'func_name' => preg_replace('/-/', '_', $module),
            'dir_name' => $module
        );
		// adds title field to ALL modules for Global Select
		array_unshift( $module_layout['sub_fields'], get_title_field( $module ));
        $pilot->layouts[] = $module_layout;
    }
}
// include global module builder
if ($pilot->use_global_modules) {
    require get_template_directory() . '/includes/modules/global_acf_def.php';
    if ((array_key_exists('page', $_GET) && $_GET['page'] != 'global-modules') || !array_key_exists('page', $_GET)) {
        $pilot->layouts[] = $global_module_layout;
    }
}

//re-include inner page
function addInnerPage($pilot){
    $module_layout = array();
    $filename = get_template_directory() . '/includes/modules/inner_page_content/functions.php';
    // module's acf layout is built and added to $pilot->layouts[] in the following require
    if (file_exists($filename)) {
        require $filename;
        $additional_fields = get_additional_fields('inner_page_content');
        foreach ($additional_fields as $field) {
            $module_layout['sub_fields'][] = $field;
        }
        $pilot->layouts[] = $module_layout;
        $pilot->module_names[] = array(
            'class' => preg_replace('/_/', '-', 'inner_page_content'),
            'key' => $module_layout['key'],
            'func_name' => preg_replace('/-/', '_', 'inner_page_content'),
            'dir_name' => 'inner_page_content'
        );
    }
}
addInnerPage($pilot);

// all layouts are added to the $pilot->flexible_content layouts key in the following require
require get_template_directory() . '/includes/modules/flexible_content_acf_def.php';
acf_add_local_field_group($pilot->flexible_content);

/* ACF Custom Functions */
function get_all_blocks($content_block_name = 'content_blocks', $is_option = false)
{
	global $pilot;
	$module_names = $pilot->module_names; // = array( 'dir-name-of-module- => array('class','key','func_name','dir_name'));
    $option = "";
    if ($is_option) {
        $option = 'option';
    }
    if (have_rows($content_block_name, $option)):
        global $i;
        $i = 0;
        while (have_rows($content_block_name, $option)):
            the_row();
            $block_type = preg_replace('/_block/', '', get_row_layout());
            $params = array('type' => $block_type);
            global $args;
            $args = array();
            $custom_classes = "";
            $color_class = " ";
            // checks if colormaker is loaded
            if (function_exists('get_color_class')) {
                if ($block_color_key = get_sub_field($block_type . '_block_color')) {
                    // checks all colors as defined in colormaker settings and matches to hidden id
                    $block_color = get_color_class($block_color_key);
                    $color_class .= $block_color . " module";
                }
            }
            if ($block_custom_classes = get_sub_field('custom_classes_' . $block_type)) {
                $custom_classes .= $block_custom_classes;
            }
			if( get_sub_field($block_type.'_include_global_classes') ){
				$custom_classes .= get_field('global_classes','option');
			}
            $args = call_user_func('build_' . $block_type . '_layout');
            if ('global' == $block_type) {
				$params['type'] = $args['global_type']; // sets the module type the global uses
				// globals are stored in wp_options as 'options_content_blocks_{$global_key}_images_0_image' 
				$params['global_key'] = get_sub_field('global_block_predefined_block');
				$custom_classes .= get_option('options_content_blocks_'.$params['global_key'].'_custom_classes_'.preg_replace('/-/','_',$args['global_type']));				
				if( get_option('options_content_blocks_'.$params['global_key'].'_'.preg_replace('/-/','_',$args['global_type']).'_include_global_classes')){
					$custom_classes .= get_field('global_classes','option');
				}
            }
            if (count($args) > 0) {
                if ($block_type != 'media') {
                    $args['id'] = $block_type . '_block_' . $i;
                }
                $params['args'] = $args;
                $params['classes'] = $custom_classes . $color_class;
                get_block($params);
            }
            $i++;
        endwhile;
    endif;
}

function get_block($params)
{
    global $pilot;
    if ($block_type = $params['type']) {
        if (array_key_exists('classes', $params)) {
            $custom_classes = $params['classes'];
        }
        $args = @$params['args'];
        $id = "";
        if (is_array($args) && array_key_exists('id', $args)) {
            $id = "id='" . $args['id'] . "'";
        }
        $block_class = preg_replace('/_/', '-', $block_type);
        $block_dir_name = get_dir_name_from_class($block_class);
        echo "<div class='block-" . $block_type . $pilot->module_classes . " " . $custom_classes . "' " . $id . "><div class='layout-content'>";
        get_template_part('includes/modules/' . $block_dir_name . '/module', 'view');
        echo "</div><!--/layout-content--></div><!--/block-->";
        unset($args);
    }
}

function get_modules()
{
    $current_directory = __FILE__;
    $root = preg_replace('#includes(.*?)modules(.*?)modules.php#', '', $current_directory);
    $path = $root . "/includes/modules";
    $dirs = glob($path . '/*', GLOB_ONLYDIR);
    $modules = array();
    foreach ($dirs as $dir) {
        $pos = strrpos($dir, '/') + 1;
        $module = substr($dir, $pos);
        if ('module-starter' != $module) {
            $modules[] = $module;
        }
    }
    return $modules;
}

function get_dir_name_from_class($class_name)
{
    global $pilot;
    foreach ($pilot->module_names as $module) {
        if ($module['class'] == $class_name) {
            return $module['dir_name'];
        }
    }
    return false;
}