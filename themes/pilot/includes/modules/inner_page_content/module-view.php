<?php
global $args;
?>
<div class="inner_page-wrap">
    <div class="inner_page_top-content">
        <?php get_all_blocks('inner_page_top_content'); ?>
    </div>
    <div class="inner_page_page-content">
        <div class="inner_page_left-content col-3 no-margin">
            <?php get_all_blocks('inner_page_left_content'); ?>
        </div>
        <div class="inner_page-right-content col-8 col-last">
            <?php get_all_blocks('inner_page_right_content'); ?>
        </div>
    </div>
    <div class="inner_page_bottom-content">
        <?php get_all_blocks('inner_page_bottom_content'); ?>
    </div>
</div>