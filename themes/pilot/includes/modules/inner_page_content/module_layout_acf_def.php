<?php
global $pilot;


function exclude_self($var)
{
    return ($var['name'] != 'inner_page_content');
}

$layouts = array_filter($pilot->layouts, "exclude_self");

// add module layout to flexible content
$module_layout = array(
    'key' => '4824f20be520',
    'name' => 'inner_page_content',
    'label' => '2-Column Layout',
    'display' => 'block',
    'sub_fields' => array(
        array(
            'key' => 'field_4824f20be521',
            'label' => 'Top Content',
            'name' => 'inner_page_top_content',
            'type' => 'flexible_content',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
            'button_label' => 'Add a Content Block',
            'min' => '',
            'max' => '',
            'layouts' => $layouts,
        ),
        array(
            'key' => 'field_4824f20be522',
            'label' => 'Left Content',
            'name' => 'inner_page_left_content',
            'type' => 'flexible_content',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
            'button_label' => 'Add a Content Block',
            'min' => '',
            'max' => '',
            'layouts' => $layouts,
        ),
        array(
            'key' => 'field_4824f20be523',
            'label' => 'Right Content',
            'name' => 'inner_page_right_content',
            'type' => 'flexible_content',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
            'button_label' => 'Add a Content Block',
            'min' => '',
            'max' => '',
            'layouts' => $layouts
        ),
        array(
            'key' => 'field_4824f20be524',
            'label' => 'Bottom Content',
            'name' => 'inner_page_bottom_content',
            'type' => 'flexible_content',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
            'button_label' => 'Add a Content Block',
            'min' => '',
            'max' => '',
            'layouts' => $layouts
        )
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
    'min' => '',
    'max' => '',
);
?>