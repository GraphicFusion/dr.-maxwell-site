<?php
/**
 * $args = array();
 */
global $args;
?>
<div class="ribbon-wrap">
    <div class="ribbon-left-content col-4">
        <?php echo $args['left_content']; ?>
    </div>
    <div class="ribbon-right-content col-8 col-last">
        <?php echo $args['right_content']; ?>
    </div>
</div>
