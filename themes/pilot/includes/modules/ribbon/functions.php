<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_ribbon_layout()
{
    $args = array(
        'left_content' => get_sub_field('ribbon_left_content'),
        'right_content' => get_sub_field('ribbon_right_content')
    );
    return $args;
}

?>