<?php
global $pilot;
// add module layout to flexible content
$module_layout = array(
    'key' => '7a1f04d18a88',
    'name' => 'ribbon',
    'label' => 'Gold Ribbon',
    'display' => 'block',
    'sub_fields' => array(
        array(
            'key' => 'field_7a1f04d18a89',
            'label' => 'Left Content',
            'name' => 'ribbon_left_content',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => 'field_7a1f04d18a90',
            'label' => 'Right Content',
            'name' => 'ribbon_right_content',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
    ),
    'min' => '',
    'max' => '',
);

?>