var menu = [
        {
            id: 'menuID',
            title: 'Menu',
            icon: 'fa fa-times',
            items: [
                {
                    name: 'Cosmetic Surgery',
                    id: 'itemID',
                    link: '#',
                    items: [
                        {
                            title: 'Cosmetic Surgery',
                            items: [
                                {
                                    name: 'Cosmetic Surgery',
                                    link: '/cosmetic-surgery'

                                },
                                {
                                    name: 'Body',
                                    link: '/cosmetic-surgery/body',
                                    items: [
                                        {
                                            title: 'Body',
                                            items: [
                                                {
                                                    name: 'Body',
                                                    link: '/cosmetic-surgery/body'
                                                },
                                                {
                                                    name: 'Arm Reduction',
                                                    link: '/cosmetic-surgery/body/arm-reduction'
                                                },
                                                {
                                                    name: 'Body Lift and Thigh Lift',
                                                    link: '/cosmetic-surgery/body/body-lift-and-thigh-lift/'
                                                },
                                                {
                                                    name: 'Brazilian Buttock Lift',
                                                    link: '/cosmetic-surgery/body/brazilian-buttock-lift/'
                                                },
                                                {
                                                    name: 'Labiaplasty',
                                                    link: '/cosmetic-surgery/body/labiaplasty/'
                                                },
                                                {
                                                    name: 'Liposuction',
                                                    link: '/cosmetic-surgery/body/liposuction/'
                                                },
                                                {
                                                    name: 'Panniculectomy',
                                                    link: '/cosmetic-surgery/body/panniculectomy/'
                                                },
                                                {
                                                    name: 'The Mommy Makeover',
                                                    link: '/cosmetic-surgery/body/the-mommy-makeover/'
                                                },
                                                {
                                                    name: 'Tummy Tuck',
                                                    link: '/cosmetic-surgery/body/the-tummy-tuck'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    name: 'Breast',
                                    link: '/cosmetic-surgery/breast',
                                    items: [
                                        {
                                            title: 'Breast',
                                            items: [
                                                {
                                                    name: 'Breast',
                                                    link: '/cosmetic-surgery/breast'
                                                },
                                                {
                                                    name: 'Breast Augmentation',
                                                    link: '/cosmetic-surgery/breast/breast-augmentation/'
                                                },
                                                {
                                                    name: 'Breast Implant Exchange',
                                                    link: '/cosmetic-surgery/breast/breast-implant-exchange/'
                                                },
                                                {
                                                    name: 'Breast Implant Removal',
                                                    link: '/cosmetic-surgery/breast/breast-implant-removal/'
                                                },
                                                {
                                                    name: 'Breast Lift',
                                                    link: '/cosmetic-surgery/breast/breast-lift/'
                                                },
                                                {
                                                    name: 'Breast Reduction',
                                                    link: '/cosmetic-surgery/breast/breast-reduction/'
                                                },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    name: 'Face',
                                    link: '/cosmetic-surgery/face',
                                    items: [
                                        {
                                            title: 'face',
                                            items: [
                                                {
                                                    name: 'Brow Lift',
                                                    link: '/cosmetic-surgery/face/brow-lift'
                                                },
                                                {
                                                    name: 'Ear Surgery',
                                                    link: '/cosmetic-surgery/face/ear-surgery'
                                                },
                                                {
                                                    name: 'Eyelid Surgery',
                                                    link: '/cosmetic-surgery/face/eyelid-surgery'
                                                },
                                                {
                                                    name: 'Facelift',
                                                    link: '/cosmetic-surgery/face/facelift'
                                                },
                                                {
                                                    name: 'Fat Injections',
                                                    link: '/cosmetic-surgery/face/fat-injections'
                                                },
                                                {
                                                    name: 'Laser Skin Resurfacing',
                                                    link: '/cosmetic-surgery/face/laser-skin-resurfacing'
                                                },
                                                {
                                                    name: 'Neck Lift',
                                                    link: '/cosmetic-surgery/face/neck-lift'
                                                },
                                                {
                                                    name: 'Nose Surgery',
                                                    link: '/cosmetic-surgery/face/nose-surgery'
                                                },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    name: 'Surgery Center',
                                    link: '/cosmetic-surgery/surgery-center'
                                },
                            ]
                        }
                    ]

                },
                {
                    name: 'Med Spa',
                    link: '#',
                    items: [
                        {
                            title: 'Med Spa',
                            items: [
                                {
                                    name: 'Med Spa',
                                    link: '/med-spa/'
                                },
                                {
                                    name: 'Face',
                                    link: '/med-spa/face-treatment',
                                    items: [
                                        {
                                            title: 'face',
                                            items: [
                                                {
                                                    name: 'Botox Dysport Xeomin',
                                                    link: '/med-spa/face-treatment/botox-dysport-xeomin/'
                                                },
                                                {
                                                    name: 'Facials and Peels',
                                                    link: '/med-spa/face-treatment/facials-and-peels/'
                                                },
                                                {
                                                    name: 'Kybella Injections',
                                                    link: '/med-spa/face-treatment/kybella-injections/'
                                                },
                                                {
                                                    name: 'Laser Treatments',
                                                    link: '/med-spa/face-treatment/laser-treatments/'
                                                },
                                                {
                                                    name: 'Micro Needling',
                                                    link: '/med-spa/face-treatment/Micro Needling/'
                                                },
                                            ]
                                        }
                                    ]                                    
                                },
                                {
                                    name: 'Body',
                                    link: '/med-spa/body',
                                    items: [
                                        {
                                            title: 'body',
                                            items: [
                                                {
                                                    name: 'Laser Treatment',
                                                    link: 'med-spa/body-treatment/laser-treatments'
                                                },
                                                {
                                                    name: 'SculpSure',
                                                    link: 'med-spa/body-treatment/sculpsure'
                                                },
                                            ]
                                        }
                                    ]                                    
                                },
                                {
                                    name: 'Hair Reduction',
                                    link: '/med-spa/hair-reduction'
                                },
                                {
                                    name: 'Products',
                                    link: '/med-spa/products'
                                },
                                {
                                    name: 'Specials',
                                    link: '/med-spa/specials'
                                }
                            ]
                        }
                    ]
                },
                {
                    name: 'Stories',
                    link: '/stories',
                    items: [
                        {
                            title: 'Stories',
                            items: [
                                {
                                    name: 'Stories',
                                    link: '/stories'
                                },
                                {
                                    name: 'Testimonials',
                                    link: '/stories/testimonials'
                                },
                                {
                                    name: 'Blog Posts',
                                    link: '/blog-posts'
                                },
                            ]
                        }
                    ]
                },
                {
                    name: 'About',
                    link: '#',
                    items: [
                        {
                            title: 'About',
                            items: [
                                {
                                    name: 'About',
                                    link: '/about'
                                },
                                {
                                    name: 'Dr. Gwen Maxwell',
                                    link: '/about/gwen-maxwell'
                                },
                                {
                                    name: 'Meet The Staff',
                                    link: '/about/meet-the-staff'
                                },
                                {
                                    name: 'In the Community',
                                    link: '/about/in-the-community'
                                }
                            ]
                        }
                    ]
                },
                {
                    name: 'Contact',
                    link: '#',
                    items: [
                        {
                            title: 'Contact',
                            items: [
                                {
                                    name: 'Contact',
                                    link: '/contact'
                                },
                                {
                                    name: 'Schedule A Consultation',
                                    link: '/contact/schedule-a-consultation'
                                },
                                {
                                    name: 'Financing Options',
                                    link: '/financing-options'
                                },
                                {
                                    name: 'Careers',
                                    link: '/careers'
                                },
                                {
                                    name: 'Resources',
                                    link: '/resources'
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
    ;
