jQuery(document).ready(function ($) {
    $menu = $('.mobile-menu'),
    $menuLabel = $('.menu-label');
    //import menu 
    $menu.multilevelpushmenu({
        menu: menu,
        collapsed: 'true',
        menuWidth: '100%',
        fullCollapse: true,
        onItemClick: function(item) {
            var event = arguments[0],
            // Second argument is menu level object containing clicked item (<div> element)
                $item = arguments[2];
            // Anchor href
            var itemHref = $item.find( 'a:first' ).attr( 'href' );
            // Redirecting the page
            location.href = itemHref;
        },
        onCollapseMenuEnd: function(item ) {
            $menuLabel.fadeIn();
        }

    }).init();
    $menuLabel.click(function() {
        $menu.multilevelpushmenu('expand');
        $(this).fadeOut();
    });

});