<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
        <span class="screen-reader-text">Search for:</span>
        <input type="search" class="search-field" placeholder="Search..." value="" name="s" title="Search for:">
    </label>
    <button type="submit" class="search-submit" value="Search"><?php get_template_part('views/icons/search'); ?></button>
</form>