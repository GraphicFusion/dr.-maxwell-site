<?php get_header('inner'); ?>
<?php if (have_posts()) : ?>


    <div class="block-inner_page_content module   " id="inner_page_content_block_0">
        <div class="layout-content">
            <div class="inner_page-wrap">
                <div class="inner_page_top-content">
                    <div class="block-generic_content module  no-margin accent-text-subtitle "
                         id="generic_content_block_0">
                        <div class="layout-content">
                            <div class="gc-wrap">
                                <?php the_archive_title('<h3 class="page-title">', '</h3>'); ?>
                                <div class="gc-content"></div>
                            </div>
                        </div><!--/layout-content--></div><!--/block-->    </div>
            </div>
        </div><!--/layout-content-->
    </div>
    <div class="stories-container">
        <?php while (have_posts()) : the_post(); ?>
            <?php $url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
            <div class="col-12 large-img-container animated fade-in">
                <div class="col-12 large-img"
                     style="background-image:url(<?php echo $url ?>);">
                    <div class="img-overlay" style="background-color:#000; opacity:0.45 "></div>
                    <div class="img-content">
                        <h4><?php the_title() ?></h4>
                        <a href=<?php the_permalink() ?>>MORE</a>
                    </div>

                </div>

            </div>
        <?php endwhile; ?>
    </div>
    <?php the_posts_navigation(); ?>
<?php endif; ?>
<?php get_footer(); ?>
