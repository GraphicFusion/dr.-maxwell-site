<?php /* Template Name: Posts Page */

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$search = isset($_GET['text']) ? $_GET['text'] : '';

$query = new Wp_Query(array(
    'post_type' => "post",
    'paged' => $paged,
    'category_name' => 'blog-posts'
));
//query for every category name, and category id

?>
<?php get_header('inner'); ?>

<?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('views/content', 'inner-page'); ?>
    <?php pilot_get_comments(); ?>

<?php endwhile; ?>
    <div class="stories-container">
    <!--div class="stories-filters col-12">
        <div class="col-4">
            <form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                <input class="search" type="text" value="" name="s" id="s" placeholder="Search"/>
            </form>
        </div>
        <div class="col-4">
            <form id="category-select" class="category-select" action="<?php echo esc_url(home_url('/')); ?>"
                  method="get">
                <?php
                $args = array(
                    'show_option_none' => __('All Categories'),
                    'show_count' => 0,
                    'orderby' => 'name',
                    'echo' => 0,
                );
                ?>
                <?php $select = wp_dropdown_categories($args); ?>
                <?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
                <?php $select = preg_replace('#<select([^>]*)>#', $replace, $select); ?>
                <?php echo $select; ?>
                <noscript>
                    <input type="submit" value="View"/>
                </noscript>
            </form>
        </div>
        <div class="col-4">
            <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo esc_attr(__('Select Month')); ?></option>
                <?php wp_get_archives(array('type' => 'monthly', 'format' => 'option', 'show_post_count' => 0)); ?>
            </select>
        </div>
    </div -->
<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
    <?php $url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
    <div class="col-12 large-img-container animated fade-in">
        <div class="col-12 large-img"
             style="background-image:url(<?php echo $url ?>);">
            <div class="img-overlay" style="background-color:#000; opacity:0.45 "></div>
            <div class="img-content">
                <h4><?php the_title() ?></h4>
                <a href=<?php the_permalink() ?>>MORE</a>
            </div>

        </div>

    </div>
<?php endwhile; ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>